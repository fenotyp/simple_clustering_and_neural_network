#!/usr/bin/env python3.5


from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
from matplotlib import pyplot as plt
from dtw import dtw
import pandas as pd
'''
from math import sin
from math import pi
from math import cos
'''

def linkagetimeseries(X, method='DTW',Y=[], length=0):
    """
    X is a table of clusters
    """
    if method == 'DTW':
        diff = 1000 #np.max(np.max(X))
        for i in range(len(X)):
            for j in range(len(X[i])):

                for m in range(i+1,len(X)):
                    for n in range(len(X[m])):
                        dist, cost, acc, path = dtw(X[i][j], X[m][n], dist=lambda x, y: np.linalg.norm(x - y, ord=1))
                        if dist <= diff: #np.abs(X[i][j] - X[m][n]) <= diff:
                            diff = dist
                            a = i
                            b = m

        cluster = X[a][:]+X[b][:]
        #print(cluster)
        X.append(cluster)
        X[a] = []
        X[b] = []

        Y.append([a, b, diff, len(cluster)])
        print("STEP: ",len(Y),"/", length)
        if all(x==[] for x in X[:-1]): # if i is empty list -> (True)
            return np.array(Y)
        else:
            return linkagetimeseries(X, method='DTW', Y=Y)

    else:
        print("Algorithm performe standard linkage from scipy.cluster.hierarchy package")
        return linkage(X, method='single')
# end linkagetimeseries


class list_of_functions():
    def __init__(self, file_name):
        self.df = pd.read_csv(file_name, sep=",")

    def head(self, nb):
        print(self.df.head(nb))

    def shape(self):
        print(self.df.shape)

    def index(self):
        return self.df.index

    def convert(self, size=None):
        #adjust data
        self.size = size
        self.X = []

        if type(self.size) == int:
            if self.size > self.df.shape[0]:
                print("Size is to large, therefore max value is used")
                self.size = int(self.df.shape[0])
        elif self.size == None:
            self.size = int(self.df.shape[0])


        for i in range(self.size):
            self.X.append([np.array(self.df.ix[i].values[:9]).reshape(-1, 1)]) # ! place to make shorter functions

    def clustering(self):
        return linkagetimeseries(self.X, method='DTW',length = len(self.X))

#df = pd.read_csv("total.csv",sep=",")
data = list_of_functions("clustering.csv")
data.head(5)
data.shape()
data.convert(200) #at the moment 600
R=data.clustering()
print(R)
plt.figure(figsize=(25, 10))
dendrogram(R, leaf_rotation=90, labels=data.index())
plt.show()

print(data.index())



"""
# EXAMPLE FOR PRINTING

df = pd.read_csv("total.csv",sep=",")
#print(df.head(5))
#print(df.shape[1])

A = df.ix[0].values
B = df.ix[4].values
C = df.ix[9].values

plt.figure(1)
plt.plot(A, '-o', label='x')
plt.plot(B, '-o', label='y')
plt.plot(C, '-o', label='z')
plt.legend(loc='upper left')

A = np.array(A).reshape(-1, 1)
B = np.array(B).reshape(-1, 1)
C = np.array(C).reshape(-1, 1)

dist, cost, acc, path = dtw(A, B, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
print(dist#, cost#, acc, path)
plt.figure(2)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

dist, cost, acc, path = dtw(A, C, dist=lambda x, z: np.linalg.norm(x - z, ord=1))
print(dist#, cost#, acc, path)
plt.figure(3)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

dist, cost, acc, path = dtw(B, C, dist=lambda y, z: np.linalg.norm(y - z, ord=1))
print(dist#, cost#, acc, path)
plt.figure(4)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

plt.show()
# END OF EXAMPLE
"""















'''
# linkage for tests
X = [[i] for i in [2.2, 8.1, 0.7, 4.8, 1.5, 9.77, 9.77, 0.2]]
print(X)
Z = linkage(X, method='single')
R = linkagetimeseries(X, method='DTW')
print(R)


print("\n\n")
print(Z)

plt.figure(figsize=(5, 2))
dendrogram(Z)
#plt.show()

plt.figure(figsize=(5, 2))
dendrogram(R)
plt.show()

'''