#!/usr/bin/env python3.5


# Importing Keras Sequential Model
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.models import load_model
import numpy

# nie ma
# from keras.layers import Activation

# Initializing the seed value to a integer.
seed = 7
numpy.random.seed(seed)

# Loading the data set (Parameters which describe set of functions)
dataset = numpy.loadtxt('total_binary.csv', delimiter=",")

# Slice input data
Y = dataset[:, 2:3]
# excpected outputs
# position [:, 0:1] -> 13.46%
# material [:, 1:2] -> 0.0%
# reaction [:, 2:3] -> 81.65%

X = dataset[:, 3:8] #6 parameter describe function
#print(Y[9])


# Start the Sequential model / 1 version
model = Sequential()
# Input 5 dim -> 1st hiden layer 20 neuronst
model.add(Dense(20, input_dim=5, kernel_initializer='uniform', activation='relu'))
# Dense mean that all layer are connected every node from one layer conect any other nodes in other layer
# 2nd and 3rd sequential hiden layer each 32 neurons
model.add(Dense(32, init='uniform', activation='relu'))
model.add(Dense(32, init='uniform', activation='relu'))

# Other way for initialization
# model = Sequential([Dense(5, input_shape=(3,), activation='relu')])
# input_shape =(784,) -> 28x28 usualy as vector it is good to have normalized data

# Dropout helps prevent overfiting.
# Dropout consists in randomly setting a fraction rate of input units to 0 at each update during training time
#model.add(Dropout(rate=0.01, noise_shape=None, seed=None))

# Output layer.
model.add(Dense(1, kernel_initializer='uniform', activation='sigmoid'))


# Compile the model
model.compile(loss='binary_crossentropy',
              optimizer='adam', metrics=['accuracy'])
# Fitting the model
model.fit(X, Y, epochs=80, batch_size=5)
# epochs -> iterations on a dataset
# batch_size -> number of samples per gradient update.

scores = model.evaluate(X, Y)

# Model returns:
# Scalar test loss (if the model has a single output and no metrics)
# or list of scalars (if the model has multiple outputs and/or metrics).
# The attribute model.metrics_names will give you the display labels for the scalar outputs.

print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

# model.summary() # print base information
print(model.summary())
model.save('model.h5')

model = load_model("model.h5")
predictdata = numpy.loadtxt('total_binary_predict.csv', delimiter=",")
PX = predictdata[:, 3:8]

print(model.predict(PX))